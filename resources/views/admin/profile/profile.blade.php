@extends('admin.layouts.master-layouts')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-sm-6 mb-4 mb-xl-0">
            <div class="d-lg-flex align-items-center">
                <div>
                    <h3 class="text-dark font-weight-bold mb-2">Hi, welcome back!</h3>
                    <h6 class="font-weight-normal mb-2">{{ Auth::user()->name }}</h6>
                </div>
                <div class="ml-lg-5 d-lg-flex d-none">
                        <button type="button" class="btn bg-white btn-icon">
                            <i class="mdi mdi-view-grid text-success"></i>
                    </button>
                        <button type="button" class="btn bg-white btn-icon ml-2">
                            <i class="mdi mdi-format-list-bulleted font-weight-bold text-primary"></i>
                        </button>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="d-flex align-items-center justify-content-md-end">
                <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-outline-inverse-info btn-icon-text">
                            Feedback
                            <i class="mdi mdi-message-outline btn-icon-append"></i>                          
                        </button>
                </div>
                <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-outline-inverse-info btn-icon-text">
                            Help
                            <i class="mdi mdi-help-circle-outline btn-icon-append"></i>                          
                    </button>
                </div>
                <div class="pr-1 mb-3 mb-xl-0">
                        <button type="button" class="btn btn-outline-inverse-info btn-icon-text">
                            Print
                            <i class="mdi mdi-printer btn-icon-append"></i>                          
                        </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Edit Profile</h4>
                  <p class="card-description">
                   
                  </p>
                  <form class="forms-sample" >
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Name</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Username" value="{{Auth::user()->name}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Email</label>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email" value="{{Auth::user()->email}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputMobile" class="col-sm-3 col-form-label">Mobile</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="exampleInputMobile" placeholder="Mobile number" value="{{Auth::user()->phone}}">
                      </div>
                    </div>                    
                    <button type="button" class="btn btn-success outline btn-lg btn-block"><i class="mdi mdi-file-check btn-icon-append"></i> Update</button>
                    
                  </form>
                </div>
              </div>
        </div>
        <div class="col-lg-6 mb-3 mb-lg-0">
            <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Reset Password</h4>
                  <p class="card-description">
                    
                  </p>
                  <form class="forms-sample">
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Old Passwod </label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="exampleInputUsername2" placeholder="Old Password">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Passwod </label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="exampleInputUsername2" placeholder="New Password">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Confirm Password </label>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Confirm Password">
                      </div>
                    </div>
                    
                    <button type="button" class="btn btn-warning btn-lg btn-block"><i class="mdi mdi-reload btn-icon-prepend"></i> Reset Password</button>
                   
                  </form>
                </div>
              </div>
        </div>
    </div>
       
</div>    
@endsection