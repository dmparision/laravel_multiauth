<nav class="bottom-navbar">
  <div class="container">
    <ul class="nav page-navigation">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('control') }}">
          <i class="mdi mdi-file-document-box menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="mdi mdi-cube-outline menu-icon"></i>
            <span class="menu-title">UI Elements</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="submenu">
              <ul>
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Buttons</a></li>
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Typography</a></li>
              </ul>
          </div>
      </li>
      <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link">
            <i class="mdi mdi-chart-areaspline menu-icon"></i>
            <span class="menu-title">Form Elements</span>
            <i class="menu-arrow"></i>
          </a>
      </li>
      <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link">
            <i class="mdi mdi-finance menu-icon"></i>
            <span class="menu-title">Charts</span>
            <i class="menu-arrow"></i>
          </a>
      </li>
      <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link">
            <i class="mdi mdi-grid menu-icon"></i>
            <span class="menu-title">Tables</span>
            <i class="menu-arrow"></i>
          </a>
      </li>
      <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link">
            <i class="mdi mdi-emoticon menu-icon"></i>
            <span class="menu-title">Icons</span>
            <i class="menu-arrow"></i>
          </a>
      </li>
      <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link">
            <i class="mdi mdi-codepen menu-icon"></i>
            <span class="menu-title">Sample Pages</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="submenu">
              <ul class="submenu-item">
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Login</a></li>
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Login 2</a></li>
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Register</a></li>
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Register 2</a></li>
                  <li class="nav-item"><a class="nav-link" href="javascript:void(0);">Lockscreen</a></li>
              </ul>
          </div>
      </li>
      <li class="nav-item">
          <a href="javascript:void(0);" class="nav-link">
            <i class="mdi mdi-file-document-box-outline menu-icon"></i>
            <span class="menu-title">Documentation</span></a>
      </li>
    </ul>
  </div>
</nav>