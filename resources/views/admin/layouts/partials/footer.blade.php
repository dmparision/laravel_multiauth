<footer class="footer">
	<div class="footer-wrap">
		<div class="w-100 clearfix">
			<span class="d-block text-center text-primary text-sm-left d-sm-inline-block">{{date('l jS \of M Y h:i:s A')}}
			</span>
			<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted &amp; made with <i class="mdi mdi-heart-outline"></i>
			</span>
		</div>
	</div>
</footer>