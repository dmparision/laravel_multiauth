@include('admin.layouts.common.header')

    @include('admin.layouts.common.navbar')

    <div class="container-fluid page-body-wrapper">
        <div class="main-panel">
            @yield('content')
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
                @include('admin.layouts.partials.footer')
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
@include('admin.layouts.common.footer')
