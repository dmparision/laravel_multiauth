
	<!-- container-scroller -->
    <!-- base:js -->
    <script src="{{ asset('theme/vendors/base/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('theme/js/template.js')}}"></script>
    <!-- endinject -->
    <!-- plugin js for this page -->
    <!-- End plugin js for this page -->
    <script src="{{ asset('theme/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{ asset('theme/vendors/progressbar.js/progressbar.min.js')}}"></script>
		<script src="{{ asset('theme/vendors/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js')}}"></script>
		<script src="{{ asset('theme/vendors/justgage/raphael-2.1.4.min.js')}}"></script>
		<script src="{{ asset('theme/vendors/justgage/justgage.js')}}"></script>
    <!-- Custom js for this page-->
    <script src="{{ asset('theme/js/dashboard.js')}}"></script>
    <!-- End custom js for this page-->
  </body>
</html>