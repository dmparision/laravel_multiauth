<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{$title}}</title>
  <!-- base:css -->
  <link rel="stylesheet" href="{{ asset('theme/vendors/mdi/css/materialdesignicons.min.css')}}">
  <link rel="stylesheet" href="{{ asset('theme/vendors/base/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('theme/css/style.css')}}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('theme/images/laravel.png')}}" />
  <style type="text/css">
    .main-panel{
      background-color: #4CAF50  !important; 
      background :url("{{asset('theme/images/dashboard/lockscreen-bg.jpg')}}");
    }
    .auth-form-light{
      /*background :url("{{asset('theme/images/auth/login-bg.jpg')}}") !important;
      background-color: #123 !important;*/
    }
  </style>
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="main-panel">
        <div class="content-wrapper d-flex align-items-center auth px-0">
          <div class="row w-100 mx-0">
            <div class="col-lg-6 mx-auto">
              <div class="auth-form-light text-left py-5 px-4 px-sm-5">
                <div class="brand-logo">
                  <img src="{{ asset('theme/images/laravel_2.png')}}" alt="logo">
                </div>
                <!-- <h4>Hello! let's get started</h4> -->

                <h6 class="font-weight-light">Sign in to continue.</h6>

                <form class="pt-3" method="POST" action="{{ route('control.login') }}">
                  {{ csrf_field() }}
                  <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                    <input id="username" type="text" class="form-control form-control-lg" name="username" value="{{ old('username') }}"  Placeholder ="Enter Email or Phone Number" autofocus>
                     @if ($errors->has('username'))
                          <span class="text-danger">
                              <strong>{{ $errors->first('username') }}</strong>
                          </span>
                      @endif                    
                  </div>
                  <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">

                    <input id="password" type="password" class="form-control form-control-lg" name="password" Placeholder="Enter Password (min length 6 char)">
                    @if ($errors->has('password'))
                        <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="mt-3">
                    <button type="submit" class="btn btn-block btn-danger btn-lg font-weight-medium auth-form-btn" > <i class="mdi mdi-login-variant"></i> SIGN IN</button>
                  </div>
                  <div class="my-2 d-flex justify-content-between align-items-center">
                    <div class="form-check">
                      <label class="form-check-label text-muted">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Keep me signed in
                      </label>
                    </div>
                    <a href="{{route('control.password.request')}}" class="auth-link text-black">Forgot password?</a>
                  </div>                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="{{ asset('theme/vendors/base/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{ asset('theme/js/template.js')}}"></script>
  <!-- endinject -->
</body>

</html>
