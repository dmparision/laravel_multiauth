<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::user())
		return redirect('/home');	
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@userLogout')->name('logout');

//admin(control) login
Route::prefix('control')->namespace('Auth\Admin')->group(function(){

	Route::get('/', 'LoginController@showLoginForm')->name('control');
	Route::post('/login', 'LoginController@loginSubmit')->name('control.login');
	Route::post('/logout', 'LoginController@logout')->name('control.logout');
	
	// Password Reset Routes... copied from router.php
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('control.password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('control.password.email');
    
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('control.password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('control.password.reset');
});

//admin dashboard and further route
Route::prefix('control')->namespace('Admin')->group(function(){
	Route::get('/dashboard','AdminController@index')->name('control.dashboard');
	Route::get('/profile','AdminController@adminProfile')->name('control.profile');
});


	
	