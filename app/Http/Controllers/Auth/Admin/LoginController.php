<?php
/*
    admin login controller
*/

namespace App\Http\Controllers\Auth\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
    
class LoginController extends Controller
{
    public function __construct()
    {
    	$this->middleware('guest:admin',['except'=>'logout']);
    }

    public function showLoginForm()
    {
        //$data['title'] = 'Admin Login';
        return view('auth.admin.login',['title' => 'Admin Login']);
    }
    public function loginSubmit(Request $request)
    {    	
        //get login credentials
        $username = $this->username($request->username);
        if( $username == 'email' ){

            $this->validate($request,[
                	'username' => 'required|email',
                	'password' => 'required|min:6'
                ],
                [
                    'username.email' => 'Please enter an valid email',
                ]
            );      
              
            if(Auth::guard('admin')->attempt(['email' => $request->username,'password' => $request->password],$request->remember)){
            	return redirect('control/dashboard');
            }else{            
            	
                throw ValidationException::withMessages([
                    'username' => [trans('auth.failed')],
                ]);
            }
        }elseif($username == 'phone'){
           
            $this->validate($request,[
                    'username' => 'required|numeric|digits_between:10,15',
                    'password' => 'required|min:6'
                ],
                [
                    'username.numeric' => 'The Username should be pure in EMAIL or PHONE NUMBER',
                    'username.digits_between' => 'The phone must be between 10 and 15 digits.',
                    'password.required' => 'Password accept some value'
                ]
            );            
            if(Auth::guard('admin')->attempt(['phone' => $request->username,'password' => $request->password],$request->remember)){
                return redirect('control/dashboard');
            }else{           
                
                throw ValidationException::withMessages([
                    'username' => [trans('auth.failed')],
                ]);
            }
        }else{
            throw ValidationException::withMessages([
                'username' => [trans('These credentials are not accepted.')],
            ]);
        }        
    }
    /*
        set user name email/phone
    */
    public function username($string=null)
    {        
        if($string == null){
            throw ValidationException::withMessages([
                'username' => [trans('Please enter an email or phone number.')],
            ]);
        }
        $username = (filter_var($string, FILTER_VALIDATE_EMAIL))?'email':'phone';
       
        return $username;
    }
    // public function showPassworReset()
    // {
    // 	return view('auth/admin/password/email');
    // }
    /*
        logout
    */
    public function logout(Request $request)
    {         
        Auth::guard('admin')->logout();
        //$request->session()->invalidate();
        return redirect('/control');
    }
}
