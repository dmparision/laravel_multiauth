<?php

namespace App\Http\Controllers\Admin;

Use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('admin/dashboard/dashboard');
        $this->data['title'] = 'Admin Dashboard';
        return view('admin.dashboard.dashboard',$this->data);
    }
    //profile
    public function adminProfile()
    {
        /*echo"<pre>";
        print_r(Auth::user());
        exit;*/

        $this->data['title'] = 'Admin Profile';
        return view('admin.profile.profile',$this->data);
    }
}
