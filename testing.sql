-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2020 at 09:28 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testing`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'AI,PK',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin user name',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin email',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin phone',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'admin password',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'rememberToken',
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=> active,0=>Inactive',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT 'created at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated at'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `phone`, `password`, `remember_token`, `active`, `created_at`, `updated_at`) VALUES
(1, 'David Johnson Maity', 'david_johnson@mailinator.com', '1234567899', '$2y$10$gBHqNtSNhqxlMSsSxEvR1Ovpx00taQStRxiIpkZvhUP5nrDN3tP0O', 'SWoqIAgNeQNQ7zrYG9aousHXbwRoRiCX1iy7N3afN2247ITiRkNJYhVz5wIF', '1', '2020-12-11 08:20:38', '2020-09-09 05:45:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_08_09_154246_create_admin_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'AI,PK',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user name',
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user code',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user email',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user phone',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'user password',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `code`, `email`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Irwin Pollich', 'TEST-5f68e1b41a028', 'hhane@example.net', '9115484843', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'niYr4a3QTr', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(4, 'Dr. Freeman Hane', 'TEST-5f68e1b41a306', 'mhyatt@example.net', '9182239050', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'Y109PVNCMp', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(5, 'Xzavier Jacobi', 'TEST-5f68e1b41a38d', 'lucile.lind@example.net', '9189096095', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'nKaZFV9oyg', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(6, 'Bettie Gusikowski', 'TEST-5f68e1b41a40d', 'adaline56@example.net', '9132638423', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'EhVW5Nm4cU', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(7, 'Russell Rath', 'TEST-5f68e1b41a48d', 'carroll.vladimir@example.com', '9155083516', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'xII1os5tIc', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(8, 'Hassie Sporer', 'TEST-5f68e1b41a4fd', 'uberge@example.org', '9169397709', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'UJXkUaFxgl', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(9, 'Dr. Dylan Emard', 'TEST-5f68e1b41a560', 'dnader@example.org', '9126794624', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'tz5ZEVfiEv', '2020-09-21 17:24:04', '2020-09-21 17:24:04'),
(10, 'Mrs. Ona Brown PhD', 'TEST-5f68e1b41a5c9', 'gleason.jules@example.com', '9160851871', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'NIfZgK5EHQ', '2020-09-21 17:24:04', '2020-09-21 17:24:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_phone_unique` (`phone`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'AI,PK', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'AI,PK', AUTO_INCREMENT=12873;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
