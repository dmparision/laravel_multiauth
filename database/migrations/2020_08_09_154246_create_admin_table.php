<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id')->comment('AI,PK');
            $table->string('name',100)->comment('admin user name');
            $table->string('email')->comment('admin email');
            $table->string('phone')->unique()->comment('admin phone');
            $table->string('password')->comment('admin password');
            $table->rememberToken()->comment('rememberToken');            
            $table->string('active')->default('1')->comment('1=> active,0=>Inactive');
            $table->timestamp('created_at')->comment('created at');
            $table->timestamp('updated_at')->nullable()->comment('updated at');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
